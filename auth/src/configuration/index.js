module.exports.PORT = process.env.PORT
module.exports.HOST = process.env.HOST
module.exports.MONGO_URL = process.env.MONGO_URL
module.exports.API_URL = process.env.API_URL