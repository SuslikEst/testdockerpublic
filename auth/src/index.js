const express = require('express')
const axios = require('axios')

const { connectDb } = require('./helpers/db')
const { PORT, HOST, API_URL } = require('./configuration')

const app = express()

app.get('/api/currentUser', (_, res) => {
    res.json({
        id: 1,
        email: 'mail@mail.ru'
    })
})

app.get('/getdatawithapi', (_, res) => {
    axios.get(`${API_URL}/testgetdatafromapi`).then((user) => {
        res.json({currentUser: user.data})
    })
})

const startServer = () => {
    app.listen(PORT, () => {
        console.log(`Server auth start success... ${HOST}:${PORT}/test`)
    })
}

connectDb()
    .on('error', console.error.bind(console, 'MongoDb connection error.'))
    .on('disconnect', connectDb)
    .once('open', () => {
        console.log('MongoDb connect success.')
        startServer()
    });