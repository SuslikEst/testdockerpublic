const mongoose = require('mongoose')
const { MONGO_URL } = require('../configuration')

module.exports.connectDb = () => {
    mongoose.connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    return mongoose.connection
}