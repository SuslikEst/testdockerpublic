const express = require('express')
const mongoose = require('mongoose')
const axios = require('axios')

const { connectDb } = require('./helpers/db')
const { PORT, HOST, AUTH_API_URL } = require('./configuration')

const app = express()

// const kittySchema = new mongoose.Schema({
//     name: String
// });
// const Kitten = mongoose.model('Kitten', kittySchema)

app.get('/test', (_, res) => {
    res.send('Hello world!')
})

app.get('/api/testgetdatafromapi', (_, res) => {
    res.json({
        emails: ['mail@mail.ru', 'serg21@ya.ru']
    })
})

app.get('/getdatawithauth', (_, res) => {
    axios.get(`${AUTH_API_URL}/currentUser`).then((user) => {
        res.json({userAuth: true, currentUser: user.data})
    })
})

const startServer = () => {
    app.listen(PORT, () => {
        console.log(`Server start success... ${HOST}:${PORT}/test`)

        // const silence = new Kitten({ name: 'Silence' })
        // silence.save(function (err, silence) {
        //     if (err) return console.error(err)
        // })

        // Kitten.find((err, kittens) => {
        //     if (err) return console.error(err);
        //     console.log('Model kittens', kittens);
        // })

    })
}

connectDb()
    .on('error', console.error.bind(console, 'MongoDb connection error.'))
    .on('disconnect', connectDb)
    .once('open', () => {
        console.log('MongoDb connect success.')
        startServer()
    });