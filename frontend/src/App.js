import './App.css'
import axios from 'axios'

function App() {

  const fetchApi = () => {
    console.log('fetchApi...')
    axios('/api/getdatawithauth').then((response) => {
      console.log('fetchApi...', response)
    })
  }

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Hello world!
        </p>
        <button onClick={fetchApi} >
          Make api request
        </button>
      </header>
    </div>
  );
}

export default App;
